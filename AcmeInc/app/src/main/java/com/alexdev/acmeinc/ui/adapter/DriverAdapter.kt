package com.alexdev.acmeinc.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.alexdev.acmeinc.R

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.alexdev.acmeinc.model.DriverWithShipment
import com.alexdev.acmeinc.util.Rules
class DriverAdapter(private val driverList: MutableList<DriverWithShipment>) : RecyclerView.Adapter<DriverAdapter.DriverViewHolder>() {

    private var itemClickListener: OnItemClickListener? = null
    interface OnItemClickListener {
        fun onItemClick(driver: DriverWithShipment)
    }
    fun setOnItemClickListener(listener: OnItemClickListener) {
        itemClickListener = listener
    }
    inner class DriverViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val nameTextView: TextView = itemView.findViewById(R.id.nameTextView)
        fun bind(driver: DriverWithShipment) {
            nameTextView.text = driver.driver.name

            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    itemClickListener?.onItemClick(driverList[position])
                }
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DriverViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.driver_list_item, parent, false)
        return DriverViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: DriverViewHolder, position: Int) {
        val driver = driverList[position]
        holder.bind(driver)
    }
    override fun getItemCount(): Int {
        return driverList.size
    }
    fun updateShipments(shipments: MutableList<String>) {
        val driverListWithShipments = driverList.mapIndexed { index, driver ->
            val shipment = Rules.findBestShipment(driver, shipments)
            if (shipment != null) {
                shipments.remove(shipment)
            }
            val suitabilityScore = Rules.calculateSuitabilityScore(driver, shipment)
            DriverWithShipment(driver.driver, shipment, suitabilityScore)
        }

        driverList.clear()
        driverList.addAll(driverListWithShipments)
        notifyDataSetChanged()
    }
}
