package com.alexdev.acmeinc.ui.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.alexdev.acmeinc.R
class DriverDetailsActivity : AppCompatActivity() {
    private lateinit var nameTextView: TextView
    private lateinit var shipmentTextView: TextView
    private lateinit var suitabilityTextView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_details)

        nameTextView = findViewById(R.id.nameTextView)
        shipmentTextView = findViewById(R.id.shipmentTextView)
        suitabilityTextView = findViewById(R.id.suitabilityTextView)

        val bundle = intent.extras
        if (bundle != null) {
            val driverName = bundle.getString("driverName")
            val shipmentDetails = bundle.getString("shipmentDetails")
            val suitabilityScore = bundle.getInt("suitabilityScore")

            nameTextView.text = driverName
            shipmentTextView.text = shipmentDetails
            suitabilityTextView.text = suitabilityScore.toString()
        }
    }
}