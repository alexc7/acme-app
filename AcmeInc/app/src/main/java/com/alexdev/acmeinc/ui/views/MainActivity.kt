package com.alexdev.acmeinc.ui.views

import JSONHelper
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alexdev.acmeinc.R
import com.alexdev.acmeinc.model.Driver
import com.alexdev.acmeinc.model.DriverWithShipment
import com.alexdev.acmeinc.ui.adapter.DriverAdapter

class MainActivity : AppCompatActivity() {

    private lateinit var driverRecyclerView: RecyclerView
    private lateinit var drivers: MutableList<DriverWithShipment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        driverRecyclerView = findViewById(R.id.driverRecyclerView)
        driverRecyclerView.layoutManager = LinearLayoutManager(this)

        val inputStream = assets.open("dataDrivers")
        val data = JSONHelper.getDriversFromJSON(inputStream)

        val shipmentsArray = data.getJSONArray("shipments")
        val driversArray = data.getJSONArray("drivers")

        val shipments: MutableList<String> = (0 until shipmentsArray.length()).map { shipmentsArray.getString(it) } as MutableList<String>
        val drivers_name: List<String> = (0 until driversArray.length()).map { driversArray.getString(it) }

        drivers = drivers_name.map { DriverWithShipment(Driver(it), "", 0) } as MutableList<DriverWithShipment>

        val adapter = DriverAdapter(drivers)
        driverRecyclerView.adapter = adapter

        adapter.updateShipments(shipments)

        adapter.setOnItemClickListener(object : DriverAdapter.OnItemClickListener {
            override fun onItemClick(driver: DriverWithShipment) {
                val intent = Intent(baseContext, DriverDetailsActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                val bundle = Bundle()
                bundle.putString("driverName", driver.driver.name)
                bundle.putString("shipmentDetails", driver.shipment)
                bundle.putInt("suitabilityScore", driver.suitabilityScore)
                intent.putExtras(bundle)
                baseContext.startActivity(intent)
            }
        })


    }
}