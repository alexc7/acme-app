package com.alexdev.acmeinc.util
import com.alexdev.acmeinc.model.DriverWithShipment
object Rules {
    fun findBestShipment(driver: DriverWithShipment, shipmentList: List<String>): String {
        var bestShipment: String = ""
        var bestScore = 0

        for (shipment in shipmentList) {
            val score = calculateSuitabilityScore(driver, shipment)
            if (score > bestScore) {
                bestShipment = shipment
                bestScore = score
            }
        }

        return bestShipment
    }
    fun calculateSuitabilityScore(driver: DriverWithShipment, shipment: String): Int {
        val shipmentLength = shipment.length

        val baseSuitabilityScore: Double = if (shipmentLength % 2 == 0) {
            driver.driver.name.count { it.toLowerCase() in "aeiou" } * 1.5
        } else {
            driver.driver.name.count { it.toLowerCase() in "bcdfghjklmnpqrstvwxyz" }.toDouble()
        }

        val commonFactors = findCommonFactors(shipmentLength, driver.driver.name.length)
        val suitabilityScore = if (commonFactors.isNotEmpty()) {
            baseSuitabilityScore * 1.5
        } else {
            baseSuitabilityScore
        }

        return suitabilityScore.toInt()
    }
    fun findCommonFactors(number1: Int, number2: Int): List<Int> {
        val factors = mutableListOf<Int>()
        val minNumber = minOf(number1, number2)

        for (i in 2..minNumber) {
            if (number1 % i == 0 && number2 % i == 0) {
                factors.add(i)
            }
        }

        return factors
    }
}