import org.json.JSONObject
import java.io.InputStream

object JSONHelper {
    fun getDriversFromJSON(inputStream: InputStream): JSONObject {
        val jsonString = inputStream.bufferedReader().use { it.readText() }
        return JSONObject(jsonString)
    }
}