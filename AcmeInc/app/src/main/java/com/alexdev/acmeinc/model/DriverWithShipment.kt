package com.alexdev.acmeinc.model
data class DriverWithShipment(
    val driver: Driver,
    val shipment: String,
    val suitabilityScore: Int
    )

